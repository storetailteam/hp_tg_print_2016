"use strict";

/// ------------------- CHARGEMENT DES RESSOURCES -----------------------
var sto = window.__sto;
var Vue = require("vue");
var $ = require("jquery");
var slick = require("./src/assets/js/slick.min.js");
var settings = require("./src/settings.json");

var commons = require("./src/commons/index.js"),
    home    = require("./src/container/home/index.js"),
    produits= require("./src/container/produits/index.js");

var oldHeight = 0;
setInterval(function() {
    var height = $('#monapp').height(), gap = 200;
    if (height !== oldHeight && ((height < oldHeight - gap)||(height > oldHeight))) {
        post("sto_changeHeight", height);
        oldHeight = height;
    }
    setRatioSlide($('#cr_products .w_slide'));
    setSquarePreviewProd($('.product .product-preview'));
    setRatioHowVideo($('#instant_ink .how-video'));

}, 16);

function setRatioSlide(select){select.css({"height":select.width() / 1.36})}
function setRatioProductSheet(select){/*select.css({"height":select.width() * 2.23})*/}
function setRatioHowVideo(select){select.css({"height":select.width() / 1.906})}
function setSquarePreviewProd(select){select.css({"height":select.width()})}
/*$( window ).resize(function() {
    setRatioSlide($('#cr_products .w_slide'));
    setRatioProductSheet($('.product_sheet'));
    setSquarePreviewProd($('.product .product-preview'));
    setRatioHowVideo($('#instant_ink .how-video'));
});*/

var all_products = { };

// POST MESSAGES & TRACKING FUNCTIONS
function post(method, value) {
    var result = {"method": method};
    if (value !== undefined){
        result.value = value;
    }
    window.parent.postMessage(JSON.stringify(result), "*");
}
function trackerMethod(trackingObj) {
    post("sto_tracking", JSON.stringify(trackingObj));
}
function trackerBasket(basket, trackingObj) {
    post("sto_trackbasket", { "basket": JSON.stringify(basket), "value": JSON.stringify(trackingObj) });
}

// SLICK COMPONENT
Vue.component('slickimages', {
    template: require("./src/container/slick/images.html"),
    props: [ 'name', 'items' ],
    beforeUpdate: function() {
        $('#cr_'+this.name).slick('unslick');
    },
    updated: function() {
        this.initSlick();
    },
    created: function() {
        this.initSlick();
    },
    methods: {
        initSlick: function() {
            var nbSlides, slidesShow;
            setRatioSlide($('#cr_'+this.name+' .w_slide'));
            setRatioProductSheet($('.'+this.name+'_sheet'));
            setSquarePreviewProd($('.'+this.name+' .'+this.name+'-preview'));
            this.$nextTick(function() {
                var carrousel = $('#cr_'+this.name);
                carrousel.slick({
                    infinite: true,
                    slidesToShow: 4,
                    dots:false,
                    arrows: false
                });
                var w_carrousel = carrousel.parent();
                if(carrousel.slick("getSlick").slideCount <= carrousel.slick("getSlick").options.slidesToShow){
                     w_carrousel.find('.slick-prev, .slick-next').hide();}
                else{w_carrousel.find('.slick-prev, .slick-next').show();}
            });
            // this.$nextTick(function() {
            //     console.log($('#cr_'+this.name).slick('slickGetOption'));
            // });
        },
        slideClick: function(item) {
            this.$emit('slideclick', item);
        },
        prevItem: function() {
            $('#cr_'+this.name).slick('slickPrev');
        },
        nextItem: function() {
            $('#cr_'+this.name).slick('slickNext');
        }
    }
});

Vue.component('slickaccess', {
    template: require("./src/container/slick/access.html"),
    props: [ 'name', 'items' ],
    beforeUpdate: function() {
        $('#cr_'+this.name).slick('unslick');
    },
    updated: function() {
        this.initSlick();
    },
    created: function() {
        this.initSlick();
    },
    methods: {
        initSlick: function() {
            setRatioSlide($('#cr_'+this.name+' .w_slide'));
            setRatioProductSheet($('.'+this.name+'_sheet'));
            setSquarePreviewProd($('.'+this.name+' .'+this.name+'-preview'));
            this.$nextTick(function() {
                var c_accessoires = $('#cr_'+this.name);
                c_accessoires.slick({
                    infinite: true,
                    slidesToShow: 3,
                    dots:false,
                    arrows: false
                });
                var w_accessoires = c_accessoires.parent();
                if(c_accessoires.slick("getSlick").slideCount <= c_accessoires.slick("getSlick").options.slidesToShow){
                    w_accessoires.find('.slick-prev, .slick-next').hide();}
                else{w_accessoires.find('.slick-prev, .slick-next').show();}
            })
        },
        slideClick: function(item) {
            this.$emit('slideclick', item);
        },
        prevItem: function() {
            $('#cr_'+this.name).slick('slickPrev');
        },
        nextItem: function() {
            $('#cr_'+this.name).slick('slickNext');
        },
        getImage: function(produit) {
            return this.$root.getImage(produit);
        },
        getAvis: function(produit) {
            return this.$root.getAvis(produit);
        },
        getPrice: function(produit) {
            return this.$root.getPrice(produit);
        },
        hasPrice: function(produit) {
            return this.$root.hasPrice(produit);
        },
        getStars: function(produit) {
            return this.$root.getStars(produit);
        },
        showFiche: function(produit) {
            return this.$root.showFiche(produit);
        },
        addToCart: function(produit) {
            return this.$root.addToCart(produit);
        },
    }
});

// AFFICHAGE DE LA HOMEPAGE
var monapp = new Vue({
    el: '#monapp',
    data: {
        state: 'init',
        page: 'home',
        produit: '',
        params: settings,
        products: {},
        popin_show: false,
        popin_content: '',
        popin_type: 'image',
        popin_pos: 'default'
    },
    beforeMount: function() {
        var t = this,
            products = settings.products,
            prequests = 0;

        // GET ALL PRODUCTS
        while(products.length)  {
            prequests++;
            $.getJSON('http://mb01.storetail.net/api/fnac/' +products.splice(0, 10).join(';'), function (data) {
                Object.keys(data).forEach(function(v) {
                    // Vérification Dispo
                    if (data[v].timestamp < (Date().now - 8 * 60 * 60))
                        data[v].disponible = false;// Vérification Dispo
                });
                Object.keys(data).forEach(function(v) {
                    all_products[v] = data[v];
                });
                prequests--;
                if (!prequests > 0) {
                    t.$set(t, 'state', 'loaded');
                }
            });
        }

    },
    methods: {
        menuChange: function(menu, from) {
            // Fire a Navigation Event
            trackerMethod({ "trackAction": "nav", "trackZone": (this.page == 'home' ? 'home' : this.params.produits[this.produit].fname), "trackLabel": (menu == 'home' ? 'home' : this.params.produits[menu].fname)+'_'+(from == 'menu' || from == 'decouvre' ? from : this.params.produits[from].fname) });

            this.$set(this, 'page', (menu == 'home' ? menu : 'page'));
            this.$set(this, 'produit', (menu == 'home' ? '' : menu));
            if (menu !== 'home') setRatioHowVideo($('#instant_ink .how-video'));
        },

        clickPDF: function(produit) {
            // Fire a PDF
            trackerMethod({ "trackAction": "clk", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "pdf", "trackLabel": produit });
        },

        clickConfigs: function(produit) {
            // Fire a Toutes les Configs
            trackerMethod({ "trackAction": "clk", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "conf", "trackLabel": produit });
        },

        closePopin: function() {
            if (this.popin_type == 'video') {
                document.getElementById('video').pause();
            }
            this.$set(this, 'popin_show', false);

            // Fire an Action Event
            trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "close", "trackLabel": "popin" });
        },

        addToCart: function(produit) {
            if (all_products[produit]) {
                // Add the product to the basket
                post("sto_addtocart", produit);

                // Fire an Add To Basket event
                trackerBasket([{
                    "id": produit,
                    "name": all_products[produit].titre.replace(/[^0-9a-z-_]/gi, '_'),
                    "price": (all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix),
                    "qty": 1,
                    "promo": true}], {"trackAction": "abk", "trackZone": this.params.produits[this.produit].fname});
            }
        },

        showFiche: function(produit) {
            // Fire a Click Event
            trackerMethod({ "trackAction": "clk", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "fiche", "trackLabel": produit });
        },

        showCaract: function(produit) {
            if($('.produit .w_view-all-btn').attr('toggle') == "false"){
                $('.w_view-all-btn').attr('toggle',"true");
                $('.w_view-all-btn a').html('Fermer les caractéristiques');
                $('.table-specs').show();

                // Fire an Action Event
                trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "open", "trackLabel": "carrousel" });
            }else{
                document.getElementById("section_prod5").scrollIntoView();
                $('.w_view-all-btn').attr('toggle',"false");
                $('.w_view-all-btn a').html('Voir toutes les caractéritiques');
                $('.table-specs').hide();

                // Fire an Action Event
                trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "close", "trackLabel": "carrousel" });
            }
            //this.$nextTick(function(){document.getElementById("section_prod5").scrollIntoView();});
        },

        slideClick: function(item, pos) {
            var position = (pos ? pos : 'default');

            this.$set(this, 'popin_pos', position);
            this.$set(this, 'popin_show', true);

            // Fire an Action Event
            trackerMethod({ "trackAction": "act", "trackZone": this.params.produits[this.produit].fname, "trackEvent": "open", "trackLabel": "popin_"+position });

            if (item.video) {
                this.$set(this, 'popin_type', 'video');
                this.$set(this, 'popin_content', produits.images[item.produit].video);
            }
            else {
                this.$set(this, 'popin_type', 'image');
                this.$set(this, 'popin_content', item.content);
            }
        },

        getImageHome: function(img, indice) {
            return home[img][indice];
        },

        getImageProduit: function(produit, type) {
            return produits.images[produit][type];
        },

        getODR: function(text) {
            if (text) {
                var odr = text.split(' ');
                odr[odr.length - 1] = odr[odr.length - 1]+'<sup>*</sup>';
                odr = odr.reduce(function(a, b) {
                    return a+'<span>'+b+'</span>'
                }, '');

                return odr;
            }
            return '';
        },

        getAccessoires: function(produit) {
            return this.params.produits[produit].page.accessoires.filter(function(e) {
                return (all_products[e]);
            }).map(function(e) {
                return all_products[e];
            });
        },

        getCarrousel: function(produit) {
            var carrousel = [];

            for(var i = 0; i < this.params.produits[produit].page.carrousel; i++) {
                carrousel.push({ produit: produit, content: produits.images[produit].carrousel[i], background: 'background-image:url('+produits.images[produit].carrousel[i]+')', video: this.params.produits[produit].page.videos.indexOf(i) !== -1 });
            }

            return carrousel;
        },

        getStars: function(produit) {
            var note = 0;
            if (all_products[produit] && all_products[produit].note) {
                note = all_products[produit].note * 10
            }

            return 'star S'+(note < 10 ? '0' : '')+note;
        },

        getAvis: function(produit) {
            var avis = 0;
            if (all_products[produit] && all_products[produit].avis) {
                avis = all_products[produit].avis;
            }

            return avis;
        },

        getImage: function(produit) {
            var image = '';
            if (all_products[produit] && all_products[produit].images) {
                image = all_products[produit].images[0]
            }

            return image;
        },

        hasPrice: function(produit) {
            if (all_products[produit]) {
                var prix = all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix;
                if (prix && all_products[produit].disponible) {
                    return true;
                }
            }
            return false;
        },

        getPrice: function(produit, type) {
            if (all_products[produit]) {
                var prix = all_products[produit].prix_barre ? all_products[produit].prix_barre : all_products[produit].prix;
                if (prix) {
                    if (type == 'full') {
                        return (prix ? ''+parseFloat(prix).toFixed(2).replace(/./g, function(c, i, a) {
                            return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c === '.' ? '<span>€' : c;
                        })+'</span>' : '');
                    }
                    else {
                        return (prix ? parseFloat(prix).toFixed(2).replace(/./g, function(c, i, a) {
                            return i && c !== "." && ((a.length - i) % 3 === 0) ? ' ' + c : c === '.' ? '<span>€' : c;
                        })+'</span>' : '');
                    }
                }
            }

            return (type == 'full' ? 'Produit momentanément indisponible' : '');
        }
    }
});
