"use strict";
var $ = require("jquery");

var style = require("./home.css");
var template = require("./home.html");
style.use();

/* DECLARE IMAGES */
var bg = [];
for(var i = 1; i < 5; i++) {
    bg.push(require('../../assets/img/HOME/img_bloc'+i+'.jpg'));
}

var sig = [];
for(var i = 1; i < 5; i++) {
//    sig.push(require('../../assets/img/HOME/signature_bloc'+i+'.png'));
    sig.push('');
}


$("nav").after(template);

export { bg, sig };
