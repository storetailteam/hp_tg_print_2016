"use strict";
var $ = require("jquery");
var style = require("./produits.css");
var template = require("./produits.html");
var settings = require("../../settings.json");
style.use();

/* DECLARE IMAGES */
var images = {};
for (var i = 0; i < settings.structure.page.fnames.length; i++) {
    var produit_id = settings.structure.page.produits[i],
        produit = settings.produits[produit_id],
        fname = settings.structure.page.fnames[i];

    images[produit_id] = {
        header:     require('../../assets/img/'+fname+'/header.jpg'),
        centre:     require('../../assets/img/'+fname+'/img_centre.jpg'),
        visuelodr:  (produit.page.odr.title ? require('../../assets/img/'+fname+'/odr.jpg') : ''),
        video:      require('../../assets/video/instant_ink.mp4')
    };
    images[produit_id]['carrousel'] = [];
    for(var j = 0; j < produit.page.carrousel; j++) {
        images[produit_id]['carrousel'].push(require('../../assets/img/'+fname+'/carrousel/carrousel_'+(j+1)+'.jpg'));
    }
};

$("nav").after(template);

export { images };
