"use strict";
__sto.load("SK+TG", function(tracker){
    tracker.display();
    const document = global.document;
    const $ = window.jQuery;
    //const baseUrl = "//localhost:8080";
    const baseUrl = "//rscdn.storetail.net/fnac/hp-2016/tg-hp-print-2016";
    const iframe = document.createElement( "IFRAME" );
    iframe.src = baseUrl;

    var sto = window.__sto;
    var controller = new Fnac.Views.ProductBuyView();

    function addtocart(data){
        controller.addBehavior({
            productId: data,
            offerId: data
        });
    }
    function trackerEvent(data){
        tracker.sendHit(JSON.parse(data.value));

    }
    function trackbasket(data){
        tracker.basket(JSON.parse(data.basket), JSON.parse(data.value));
    }

    window.addEventListener("message", function(e) {
        var data;
        try {
            data = JSON.parse(e.data);
        }
        catch(e) {
            return ;
        }
        switch (data.method) {
            case "sto_changeHeight":
                // if (window.navigator.userAgent.indexOf("Edge") < 0) {
                $('#sto_iframe').css(
                    {height: data.value + 'px'}
                );
                // }
                break;
            case "sto_tracking":
                trackerEvent(data);
                break;
            case "sto_addtocart":
                addtocart(data.value);
                break;
            case "sto_trackbasket":
                trackbasket(data.value);
                break;
        }
    });


    $( function () {

        // const $content = $( ".Main" );
        //$( ".Main .strate, .Main .StrateAsync, .Main .js-recoPartners, .Main .reassurance, .Main .stratePlayer, .content").detach();
        // $( "#wrapper" ).css( {
        //     "min-height": "782px",
        //     "overflow": "auto"
        // } );
        //
        // $content.css( "min-height", "782px" );
        $( "<iframe id='sto_iframe' src='"+baseUrl+"' scrolling='no' />" )
            .css({
                background: "rgb(207, 209, 215)",
                width: "100%",
                "max-width":"2000px",
                height: (window.navigator.userAgent.indexOf("Edge") > -1 ? "8000px" : "848px"),
                border: "none",
                margin: "0 auto",
                display: "block"
            })
            .prependTo( ".Main .content" );
    } );

});
