"use strict";

const
    path = require( "path" ),
    webpack = require( "webpack" ),
    node_modules = path.resolve( __dirname, "node_modules" );

//assumes your in dev if no production flag is present
//if one is present webpack will process to automatic
//optimisation. we just have to pull the right config file
const isDev = process.argv.indexOf( "-p" ) === -1;

module.exports = {
    devtool: "source-map",
    plugins: [
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
       })
   ],
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style/useable!css'
            }, {
                test: /\.(png|jpg|ttf|woff|eot|mp4)$/,
                loader: 'file-loader'
            }, {
                test: /\.html$/,
                loader: 'html-loader'
            }, {
                test: /\.json$/,
                loader: 'json-loader'
            }, {
                test: /\.js$/,
                loader: "babel-loader",
                query: {
                    cacheDirectory: true,
                    presets: [ "es2015" ]
                },
                excludes: [
                    node_modules
                ]
            },
            { test: require.resolve("jquery"), loader: "imports?jQuery=jquery" }
        ]
    },
    plugins: [
        new webpack.DefinePlugin( {
            "process.env": {
                "NODE_ENV": JSON.stringify( isDev ? "development" : "production" )
            }
        } )
    ],
    entry: {
        tg: "./tg.js",
        script: "./script.js" // le script qui charge la tg, appelé au travers de l'adserver
    },
    output: {
        path: "build",
        publicPath: "build/",
        filename: "[name].bundle.js"
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.js"
        }
    }
};
